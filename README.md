# SuperHeroApi

_Primer acercamiento a SwiftUI consumiendo api de heroes_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Xcode Version 11.7
Swift 5.1+
Alamofire ~> 5.2
SwiftyJSON ~> 4.0
SDWebImageSwiftUI
API: https://www.superheroapi.com/
```

### Caracteristicas 🔧

_En este pequeño demo encontraras algunos ejemplos de consumit la api de heroes_
```
*Listado de heroes al azar 
*Detalle de cada heroe
*Busqueda de heroe desde la api
```
### Algunos puntos de interes sobre la api ⌨️

_Al no tener una consulta por el total de heroes se rrecurrio obtener la primer lista al cargar por primera vez la app mediante una consulta de una letra aleatoria para poder generar los datos desde la peticion de busqueda, asi se cuenta con una lista al azar dependiendo la letra optenida_


## Mejorar 📦

_Pulir metodos y mejorar el manejo de los datos entre otros_
