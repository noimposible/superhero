//
//  ListHeroView.swift
//  SuperHero
//
//  Created by Noimposible on 08/09/20.
//  Copyright © 2020 Noimposible. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI


struct ListHeroView: View {
    
    @ObservedObject var datos = NetworkinManager()
    
    
    @State private var datoBusqueda: String = ""
    
    
    var body: some View {
        
        VStack {
          
            HStack{
                
                TextField(" ", text: $datoBusqueda).textFieldStyle(RoundedBorderTextFieldStyle())
                Button(action: {
                    self.datos.buscarDatos(dato:self.datoBusqueda)
                }){
                    Text("Buscar")
                }
               
            }.padding(10)
            ScrollView(.horizontal) {
                HStack(spacing: 20) {
                    ForEach(datos.heroTop) { hero in
                        NavigationLink(destination: HeroDetailView(hero: hero)){
                        HeroViewTop(heroe: hero)
                        }
                    }
                }.frame(minHeight: 0, maxHeight: .greatestFiniteMagnitude)
            }.frame(height: 116)
            
            List(datos.heroList){ hero in
                
                NavigationLink(destination: HeroDetailView(hero: hero)){
                    HStack{
                        VStack(alignment: .leading){
                            
                            Text(hero.name)
                                .font(.title)
                                .bold()
                            Text(hero.full_name)
                                .font(.subheadline)
                                .bold()
                            
                        }
                    }.navigationBarTitle("HEROES", displayMode: .inline)
                }
           }
           }
            
        }
    }
    


struct ListHeroView_Previews: PreviewProvider {
    static var previews: some View {
        ListHeroView()
    }
}
