//
//  HeroDetailView.swift
//  SuperHero
//
//  Created by Noimposible on 08/09/20.
//  Copyright © 2020 Noimposible. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI


struct HeroDetailView: View {
    
    let hero: HeroList
    
    var body: some View {
        
        VStack{
            
            AnimatedImage(url: URL(string: hero.image))
                .resizable()
                .frame(width: 70, height: 70)
                .clipShape(Circle())
            Text(hero.name)
                .font(.title)
                .fontWeight(.medium)
            Form{
                Section{
                    HStack{
                        Text("Nombre de Super Heroe")
                        Spacer()
                        Text(hero.name)
                            .foregroundColor(.gray)
                            .font(.callout)
                    }
                    HStack {
                        Text("Nombre Real")
                        Spacer()
                        Text(hero.full_name)
                            .foregroundColor(.gray)
                            .font(.callout)
                    }
                    HStack {
                        Text("Alter Egos")
                        Spacer()
                        Text(hero.alter_egos)
                            .foregroundColor(.gray)
                            .font(.callout)
                            .frame(width: 180)
                    }
                }
                Section{
                  HStack{
                        Text("Alias")
                        Spacer()
                    Text(hero.aliases)
                            .foregroundColor(.gray)
                            .font(.callout)
                    }
                    HStack {
                        Text("Grupo")
                        Spacer()
                        Text(hero.group_affiliation)
                            .foregroundColor(.gray)
                            .font(.callout)
                    }
                    HStack {
                        Text("Parientes")
                        Spacer()
                        Text(hero.relatives)
                            .foregroundColor(.gray)
                            .font(.callout)
                    }
                }
            }
        }.padding(4)
    }
}

