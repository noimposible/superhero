//
//  ContentView.swift
//  SuperHero
//
//  Created by Noimposible on 08/09/20.
//  Copyright © 2020 Noimposible. All rights reserved.
//

import SwiftUI


struct ContentView: View {
    
   
    var body: some View {
        NavigationView{
                ListHeroView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
