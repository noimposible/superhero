//
//  UserList.swift
//  AlamofireAPI
//
//  Created by Jorge Maldonado Borbón on 22/11/19.
//  Copyright © 2019 Jorge Maldonado Borbón. All rights reserved.
//

import Foundation

struct HeroList: Identifiable {
    var id = UUID()
    var numero : Int
    var name : String
    var image : String
    var full_name : String
    var alter_egos : String
    var aliases : String
    var group_affiliation : String
    var relatives : String
   
}
