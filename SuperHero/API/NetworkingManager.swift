//
//  NetworkingManager.swift
//  SuperHero
//
//  Created by Noimposible on 08/09/20.
//  Copyright © 2020 Noimposible. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Combine



class NetworkinManager: ObservableObject {
    
    @Published var heroList = [HeroList]()
    @Published var heroTop = [HeroList]()
    
    init() {
        if(heroList.count == 0){
            
            todosDatos()
        }

    }
    
    func todosDatos(){
        DispatchQueue.main.async {
            debugPrint("INICIO REQUEST")
            var letra :String = ""
            letra =  self.randomString()
            debugPrint(letra)
            AF.request("https://www.superheroapi.com/api.php/10156112965520834/search/\(letra)").responseJSON { (response) in
               
                switch response.result {
                case .success(let value):
                    var i :Int = 0
                    let json = JSON(value)
                    for item in json["results"] {
                        
                        i = i+1
                        let name = item.1["name"].string ?? ""
                        let image = item.1["image"]["url"].string ?? ""
                        let full_name = item.1["biography"]["full-name"].string ?? ""
                        let alter_egos = item.1["biography"]["alter-egos"].string ?? ""
                        let aliases = item.1["biography"]["aliases"][0].string ?? ""
                        let group_affiliation = item.1["connections"]["group-affiliation"].string ?? ""
                        let relatives = item.1["connections"]["relatives"].string ?? ""
                        
                        let heroes = HeroList( numero: i ,name: name,image: image,full_name:full_name,alter_egos:alter_egos, aliases: aliases, group_affiliation: group_affiliation ,relatives:relatives  )
                        
                        if(i < 5){
                            self.heroTop.append(heroes)
                        }
                        self.heroList.append(heroes)
                        
                        
                    }
                case .failure(let error):
                    print(error)
                }
                
            }
        }
    }
    
    func buscarDatos(dato:String) {
        if(!dato.isEmpty ){
        heroList = []
        DispatchQueue.main.async {
            
            
            AF.request("https://www.superheroapi.com/api.php/10156112965520834/search/\(dato.uppercased())").responseJSON { (response) in
                
                switch response.result {
                    
                case .success(let value):
                    debugPrint("BUSCAR DATOS ")
                    var i :Int = 0
                    let json = JSON(value)
                    for item in json["results"] {
                        
                        i = i+1
                        let name = item.1["name"].string ?? ""
                        let image = item.1["image"]["url"].string ?? ""
                        let full_name = item.1["biography"]["full-name"].string ?? ""
                        let alter_egos = item.1["biography"]["alter-egos"].string ?? ""
                        let aliases = item.1["biography"]["aliases"][0].string ?? ""
                        let group_affiliation = item.1["connections"]["group-affiliation"].string ?? ""
                        let relatives = item.1["connections"]["relatives"].string ?? ""
                        
                        if(name.uppercased()) == (dato.uppercased()){
                            
                        let heroes = HeroList( numero: i ,name: name,image: image,full_name:full_name,alter_egos:alter_egos, aliases: aliases, group_affiliation: group_affiliation ,relatives:relatives  )
                        
                            debugPrint(name)
                            debugPrint("Somo Iguales")
                            
                            self.heroList.append(heroes)
                        }
                        
                       
                    }
                case .failure(let error):
                    print(error)
                }
                
            }
            }
        }else{
           todosDatos()
        }
    }
    
    func randomString() -> String {
        let letters = "abcdefghijklmnopqrstuvwxyz"
        return String((0..<1).map{ _ in letters.randomElement()! })
    }
    
    
}
