//
//  HeroView.swift
//  SuperHero
//
//  Created by Noimposible on 08/09/20.
//  Copyright © 2020 Noimposible. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI


struct HeroViewTop: View {
    
    let heroe: HeroList
    
    var body: some View {
        
        VStack{
           AnimatedImage(url: URL(string: heroe.image))
               .resizable()
               .frame(width: 70, height: 70)
               .clipShape(Circle())
            Text(heroe.name)
            .frame(width: 100)
            .lineLimit(1)
        }.padding(1)
    }
}


